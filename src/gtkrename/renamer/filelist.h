/*
 *    librenamer - a simple library for batch renaming
 *    Copyright (C) 2008, 2009  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef FILELIST_H__
#define FILELIST_H__

#include <glib.h>
#include <renamer/file.h>

typedef struct FileList FileList;

FileList* filelist_new_from_argv(int argc, char* argv[]);
void filelist_free(FileList* list);

void filelist_print(FileList* filelist);

size_t filelist_length(FileList* filelist);

FileList* filelist_grep(FileList* filelist, const char* regexp);
FileList* filelist_sed(FileList* filelist, const char* fromexpr, const char* toexpr);

gboolean filelist_can_rename(FileList* srcfiles, FileList* dstfiles);
void filelist_perform_rename(FileList* srcfiles, FileList* dstfiles);

FileList* filelist_clone(const FileList* filelist);

typedef void (*filelist_foreach_file_callback)(File* file);
void filelist_foreach_file(FileList* filelist, filelist_foreach_file_callback callback);
typedef void (*filelist_pairs_foreach_file_callback)(File* file, File* file2);
void filelist_pairs_foreach_file(FileList* filelist1, FileList* filelist2, filelist_pairs_foreach_file_callback callback);

#endif  /* FILELIST_H__ */

