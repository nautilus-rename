/*
 *    librenamer - a simple library for batch renaming
 *    Copyright (C) 2008, 2009  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <glib.h>
#include <renamer/filelist.h>

struct FileList
{
	GList* list;
};

void filelist_foreach_file(FileList* filelist, filelist_foreach_file_callback callback)
{
	assert(filelist);

	for(GList* it = filelist->list; it != NULL; it = it->next) {
		assert(it->data);
		callback(it->data);
	}
}

static FileList* filelist_new()
{
	FileList* filelist = g_new0(FileList, 1);
	filelist->list = NULL;
	return filelist;
}

FileList* filelist_new_from_argv(int argc, char* argv[])
{
	FileList* filelist = filelist_new();

	for(int i = 1; i < argc; ++i) {
		File* f = file_new(argv[i]);
		if (FALSE == file_exists(f)) {
			file_free(f);
			filelist_free(filelist);
			return NULL;
		}
		filelist->list = g_list_append(filelist->list, f);
	}

	return filelist;
}

void filelist_print(FileList* filelist)
{
	void print(File* file) {
		printf("%s %s\n", file_dirname(file), file_filename(file));
	}

	filelist_foreach_file(filelist, print);
}

size_t filelist_length(FileList* filelist)
{
	return g_list_length(filelist->list);
}

void filelist_free(FileList* filelist)
{
	assert(filelist);

	filelist_foreach_file(filelist, file_free);
	g_list_free(filelist->list);

	g_free(filelist);
}

static void filelist_add_file(FileList* filelist, const File* file)
{
	assert(filelist);
	assert(file);
	filelist->list = g_list_append(filelist->list, file_clone(file));
}

FileList* filelist_clone(const FileList* filelist)
{
	FileList* newfilelist = filelist_new();

	void clone_and_add(File* file)
	{
		assert(file);
		filelist_add_file(newfilelist, file_clone(file) );
	}

	filelist_foreach_file((FileList*)filelist, clone_and_add);
	return newfilelist;
}

FileList* filelist_grep(FileList* filelist, const char* regexpstr)
{
	FileList* newfilelist = filelist_new();

	void add_if_matching(File* file)
	{
		assert(file);
		if (g_regex_match_simple(regexpstr, file_filename(file), 0, 0)) {
			filelist_add_file(newfilelist, file_clone(file));
		}
	}

	filelist_foreach_file(filelist, add_if_matching);

	return newfilelist;
}

FileList* filelist_sed(FileList* filelist, const char* fromexpr, const char* toexpr)
{
	FileList* newfilelist = filelist_new();

	GRegex* matchregex = g_regex_new(fromexpr, 0, 0, NULL);

	if (NULL == matchregex || !strcmp(fromexpr, "")) {
		return newfilelist;
	}

	void sed_file(File* file)
	{
		const char* dir = file_dirname(file);

		char* newfilename = g_regex_replace(matchregex, file_filename(file), -1, 0, toexpr, 0, NULL);
		char* newpath = g_build_filename(dir, newfilename, NULL);
		g_free(newfilename);

		File* newfile = file_new(newpath);
		g_free(newpath);

		filelist_add_file(newfilelist, newfile);
	}

	filelist_foreach_file(filelist, sed_file);

	g_regex_unref(matchregex);

	return newfilelist;
}

static gboolean filelist_has_duplicates(FileList* filelist)
{
	for(GList* it1 = filelist->list; it1 != NULL; it1 = it1->next) {
		for(GList* it2 = filelist->list; it2 != NULL; it2 = it2->next)
			if (it1 == it2) {
				continue;
			} else {
				char* path1 = file_buildpath(it1->data);
				char* path2 = file_buildpath(it2->data);

				gboolean equal = !strcmp(path1, path2);
				g_free(path1);
				g_free(path2);

				if (equal) {
					return TRUE;
				}
			}
	}

	return FALSE;
}

typedef enum {ES_NONE_EXIST, ES_ALL_EXIST, ES_NEITHER} ExistStatus;

static ExistStatus filelist_exist_status(FileList* filelist)
{
	int nexists = 0;

	void check_if_exists(File* file) {
		if (file_exists(file)) {
		    nexists++;
		}
	}
	filelist_foreach_file(filelist, check_if_exists);

	if (filelist_length(filelist) == nexists) {
		return ES_ALL_EXIST;
	} else if (0 == nexists) {
		return ES_NONE_EXIST;
	} else {
		return ES_NEITHER;
	}
}

gboolean filelist_can_rename(FileList* srcfiles, FileList* dstfiles)
{
	if (filelist_length(srcfiles) != filelist_length(dstfiles)) {
		return FALSE;
	} else if (filelist_has_duplicates(srcfiles) || filelist_has_duplicates(dstfiles)) {
		return FALSE;
	} else if (ES_ALL_EXIST != filelist_exist_status(srcfiles) || ES_NONE_EXIST != filelist_exist_status(dstfiles)) {
		return FALSE;
	} else {
		return TRUE;
	}
}

void filelist_perform_rename(FileList* srcfiles, FileList* dstfiles)
{
	if (FALSE == filelist_can_rename(srcfiles, dstfiles)) {
	    g_error("Please call filelist_can_rename (and get a value \"true\" back) before calling this function");
	    exit(-1);
	}

	void rename_file(File* src, File* dst)
	{
		char* srcpath = file_buildpath(src);
		char* dstpath = file_buildpath(dst);

		printf("mv %s %s \n", srcpath, dstpath);
		rename(srcpath, dstpath);

		g_free(srcpath);
		g_free(dstpath);
	}

	filelist_pairs_foreach_file(srcfiles, dstfiles, rename_file);
}

void filelist_pairs_foreach_file(FileList* filelist1, FileList* filelist2, filelist_pairs_foreach_file_callback callback)
{
	GList* it = filelist1->list;
	GList* it2 = filelist2->list;

	for(; it != NULL && it2 != NULL;) {
		callback(it->data, it2->data);
		it = it->next;
		it2 = it2->next;
	}
}

