/*
 *    librenamer - a simple library for batch renaming
 *    Copyright (C) 2008, 2009  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <assert.h>
#include <glib.h>
#include <renamer/file.h>

struct File
{
	char* filename;
	char* dirname;
};

File* file_new(const char* fullpath)
{
	File* file = g_new0(File, 1);
	file->dirname  = g_path_get_dirname(fullpath);
	file->filename = g_path_get_basename(fullpath);
	return file;
}

File* file_clone(const File* file)
{
	File* newfile = g_new0(File, 1);
	newfile->dirname  = g_strdup(file->dirname);
	newfile->filename = g_strdup(file->filename);
	return newfile;
}

void file_free(File* file)
{
	assert(file);
	g_free(file->filename);
	g_free(file->dirname);
	g_free(file);
}

const char* file_filename(const File* file)
{
	return file->filename;
}

const char* file_dirname(const File* file)
{
	return file->dirname;
}

gboolean file_exists(const File* file)
{
    char* fullpath = file_buildpath(file);
    gboolean do_exists = g_file_test(fullpath, G_FILE_TEST_EXISTS);
    g_free(fullpath);
    return do_exists;
}

char* file_buildpath(const File* file)
{
    return g_build_filename(file_dirname(file), file_filename(file), NULL);
}

