/*
 *    gtkrename - a simple gui for batch renaming
 *    Copyright (C) 2008, 2009  Kristian Rumberg (kristianrumberg@gmail.com)
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <renamer/filelist.h>

typedef struct {
	FileList* orig_filelist;
	FileList* matched_filelist;

	GtkWindow* window;
	GtkTreeView* matched_view;
	GtkEntry* match_entry;
	GtkTreeView* replaced_view;
	GtkEntry* replace_entry;
	GtkButton* cancel_button;
	GtkButton* apply_button;
} GUIData;

static GtkWidget* glade_xml_get_widget_or_die(GladeXML* xml, const char* id)
{
	GtkWidget* widget = glade_xml_get_widget(xml, id);
	if (NULL == widget) {
		g_error("Unable to get pointer to component %s", id);
		exit(-1);
	}
	return widget;
}

static void set_text_model_for_view(GtkTreeView* tview)
{
	gtk_tree_view_set_model(tview, GTK_TREE_MODEL(gtk_list_store_new(1, G_TYPE_STRING)));
	GtkCellRenderer* r = gtk_cell_renderer_text_new();
	GtkTreeViewColumn* p_column = gtk_tree_view_column_new_with_attributes ("Filenames", r, "text", 0, NULL);
	gtk_tree_view_insert_column(tview, p_column, 0);
	gtk_tree_view_set_rules_hint(tview, TRUE);
}

static gboolean delete_event( GtkWidget *widget, GdkEvent  *event, gpointer data)
{
	return FALSE;
}

static void destroy( GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
}

static void update_textview(GtkTreeView* view, FileList* filelist)
{
	GtkListStore* store = GTK_LIST_STORE(gtk_tree_view_get_model(view));
	gtk_list_store_clear(store);

	GtkTreeIter iter;

	void append(File* file)
	{
		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter, 0, file_filename(file), -1);
	}

	filelist_foreach_file(filelist, append);
	gtk_tree_view_expand_all(view);
}

static void show_message_with_typeflag(const char* message, const char* title, unsigned typeflag)
{
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new(NULL,
			GTK_DIALOG_DESTROY_WITH_PARENT,
			typeflag,
			GTK_BUTTONS_OK,
			"%s",
			message);
	gtk_window_set_title(GTK_WINDOW(dialog), title);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

static void show_info(const char* message, const char* title)
{
	show_message_with_typeflag(message, title, GTK_MESSAGE_INFO);
}

static void show_error(const char* message)
{
	show_message_with_typeflag(message, "Error", GTK_MESSAGE_ERROR);
}

static void try_activate_apply_button(GUIData* gdata, FileList* replaced_filelist)
{
	gboolean eflag = FALSE;
	if (filelist_can_rename(gdata->matched_filelist, replaced_filelist)) {
		eflag = TRUE;
	}
	gtk_widget_set_sensitive(GTK_WIDGET(gdata->apply_button), eflag);
}

static void perform_renames(GtkWidget* widget, gpointer data)
{
	GUIData* gdata = (GUIData*) data;

	const char* matchedtext = gtk_entry_get_text(gdata->match_entry);
	const char* replacedtext = gtk_entry_get_text(gdata->replace_entry);

	FileList* replaced_filelist = filelist_sed(gdata->matched_filelist, matchedtext, replacedtext);
	filelist_perform_rename(gdata->matched_filelist, replaced_filelist);
	filelist_free(replaced_filelist);

	show_info("Done renaming :)", "Completed");
	exit(0);
}

static void update_replacedview(GtkWidget* widget, gpointer data)
{
	GUIData* gdata = (GUIData*) data;

	const char* matchedtext = gtk_entry_get_text(gdata->match_entry);
	const char* replacedtext = gtk_entry_get_text(gdata->replace_entry);

	FileList* replaced_filelist = filelist_sed(gdata->matched_filelist, matchedtext, replacedtext);

	update_textview(gdata->replaced_view, replaced_filelist);

	try_activate_apply_button(gdata, replaced_filelist);

	filelist_free(replaced_filelist);
}

static void update_matchedview(GtkWidget *widget, gpointer data)
{
	GUIData* gdata = (GUIData*) data;

	filelist_free(gdata->matched_filelist);

	const char* matchedtext = gtk_entry_get_text(gdata->match_entry);

	gdata->matched_filelist = filelist_grep(gdata->orig_filelist, matchedtext);
	update_textview(gdata->matched_view, gdata->matched_filelist);

	update_replacedview(NULL, gdata);
}

static void gui_init(int argc, char* argv[])
{
	gtk_init(&argc,&argv);
	glade_init();

	FileList* orig_filelist = filelist_new_from_argv(argc, argv);
	if (NULL == orig_filelist) {
		show_error("Error: All files passed to the program must exists");
		exit(-1);
	}

	GladeXML* xml = glade_xml_new("/usr/share/gtkrename/glade/gtkrename.glade", NULL, NULL);
	if (NULL == xml) {
		xml = glade_xml_new("gtkrename.glade", NULL, NULL);
		if (NULL == xml) {
			g_error("Unable to load glade file");
			exit(-1);
		}
	}

	GUIData* gdata    = g_new0(GUIData, 1);

	gdata->orig_filelist    = orig_filelist;
	gdata->matched_filelist = filelist_clone(orig_filelist);

	gdata->window	= GTK_WINDOW( glade_xml_get_widget_or_die(xml, "mainwindow") );
	g_signal_connect (G_OBJECT(gdata->window), "delete_event", G_CALLBACK (delete_event), NULL);
	g_signal_connect (G_OBJECT(gdata->window), "destroy", G_CALLBACK (destroy), NULL);

	gdata->matched_view  = GTK_TREE_VIEW( glade_xml_get_widget_or_die(xml, "matched_files_view") );
	set_text_model_for_view(gdata->matched_view);

	gdata->replaced_view = GTK_TREE_VIEW( glade_xml_get_widget_or_die(xml, "replaced_files_view") );
	set_text_model_for_view(gdata->replaced_view);

	gdata->match_entry   = GTK_ENTRY( glade_xml_get_widget_or_die(xml, "match_entry") );
	g_signal_connect(G_OBJECT(gdata->match_entry), "changed", G_CALLBACK(update_matchedview), gdata);

	gdata->replace_entry = GTK_ENTRY( glade_xml_get_widget_or_die(xml, "replace_entry") );
	g_signal_connect(G_OBJECT(gdata->replace_entry), "changed", G_CALLBACK(update_replacedview), gdata);

	gdata->cancel_button = GTK_BUTTON( glade_xml_get_widget_or_die(xml, "cancel_button"));
	g_signal_connect(G_OBJECT(gdata->cancel_button), "clicked", G_CALLBACK(destroy), NULL);

	gdata->apply_button = GTK_BUTTON( glade_xml_get_widget_or_die(xml, "apply_button"));
	g_signal_connect(G_OBJECT(gdata->apply_button), "clicked", G_CALLBACK(perform_renames), gdata);

	update_matchedview(NULL, gdata);

	gtk_widget_show_all(GTK_WIDGET(gdata->window));
	gtk_main();
}

int main(int argc, char* argv[])
{
	gui_init(argc, argv);

	return 0;
}

