/*
 *  nautilus-rename.h
 *
 *  Copyright (C) 2008 Kristian Rumberg
 *  Copyright (C) 2004-2005 Jürg Billeter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Kristian Rumberg <kristianrumberg@gmail.com>,
 *	  based on nautilus-image-converter written by Jürg Billeter <j@bitron.ch>
 *
 */

#ifndef NAUTILUS_RENAME_H
#define NAUTILUS_RENAME_H

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_TYPE_RENAME	  (nautilus_rename_get_type ())
#define NAUTILUS_RENAME(o)		  (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_TYPE_RENAME, NautilusRename))
#define NAUTILUS_IS_RENAME(o)	  (G_TYPE_CHECK_INSTANCE_TYPE ((o), NAUTILUS_TYPE_RENAME))
typedef struct _NautilusRename	  NautilusRename;
typedef struct _NautilusRenameClass	  NautilusRenameClass;

struct _NautilusRename {
	GObject parent_slot;
};

struct _NautilusRenameClass {
	GObjectClass parent_slot;
};

GType nautilus_rename_get_type (void);
void  nautilus_rename_register_type (GTypeModule *module);

G_END_DECLS

#endif
