/*
 *  nautilus-rename.c
 *
 *  Copyright (C) 2008 Kristian Rumberg
 *  Copyright (C) 2004-2005 Jürg Billeter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Kristian Rumberg <kristianrumberg@gmail.com>,
 *	  based on nautilus-image-converter written by Jürg Billeter <j@bitron.ch>
 *
 */

#include <string.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libnautilus-extension/nautilus-file-info.h>
#include <libnautilus-extension/nautilus-menu-provider.h>
#include "nautilus-rename.h"

static void nautilus_rename_instance_init (NautilusRename      *img);
static void nautilus_rename_class_init    (NautilusRenameClass *class);

static GType rename_type = 0;

static gboolean rename_file_is_renamable (NautilusFileInfo *file_info)
{
	#if 0
	gchar* uri_scheme;
	gchar* mime_type;
	gboolean maybe_rename;

	maybe_rename = TRUE;
	uri_scheme = nautilus_file_info_get_uri_scheme (file_info);
	if (strcmp (uri_scheme, "file") != 0)
		maybe_rename = FALSE;
	g_free (uri_scheme);

	mime_type = nautilus_file_info_get_mime_type (file_info);
	if (strncmp (mime_type, "image/", 6) != 0)
		maybe_rename = FALSE;
	g_free (mime_type);

	return maybe_rename;
	#else
	return TRUE;
	#endif
}

#if 0
static GList* rename_filter_renames (GList *files)
{
	GList *renames;
	GList *file;

	renames = NULL;

	for (file = files; file != NULL; file = file->next) {
		if (rename_file_is_renamable (file->data))
			renames = g_list_prepend (renames, file->data);
	}

	return renames;
}
#endif

static void rename_callback (NautilusMenuItem *item,
				    GList *files)
{
	char** argv = g_new0(char*, g_list_length(files) + 2);

	int c = 0;
	argv[c++] = "/usr/bin/gtkrename";
	for(GList* it = files; it != NULL; it = it->next) {
		char *uri = nautilus_file_info_get_uri (it->data);
		char *filename = gnome_vfs_get_local_path_from_uri (uri);
		g_free(uri);
		/*GFile* orig_location = nautilus_file_info_get_location (it->data);*/
		//char* path = nautilus_file_info_get_uri(it->data);
		argv[c++] = filename;
	}
	argv[c] = NULL;

	char* currdir = g_get_current_dir();
	g_spawn_async(currdir, argv, NULL, 0, NULL, NULL, NULL, NULL);
	g_free(currdir);

	for(int i = 1; argv[i] != NULL; ++i) {
		g_free(argv[i]);
	}

	g_free(argv);
}

static GList* nautilus_rename_get_background_items (NautilusMenuProvider *provider,
						    GtkWidget		  *window,
						    NautilusFileInfo	  *file_info)
{
	return NULL;
}

GList* nautilus_rename_get_file_items (NautilusMenuProvider *provider,
				       GtkWidget	    *window,
				       GList		*files)
{
	NautilusMenuItem* item;
	GList* file;
	GList* items = NULL;

	for (file = files; file != NULL; file = file->next) {
		if (rename_file_is_renamable (file->data)) {
			item = nautilus_menu_item_new ("NautilusRename::rename",
						       "_Mass rename...",
						       "Rename each selected file",
						       "stock_interaction");
			g_signal_connect (item, "activate",
					  G_CALLBACK (rename_callback),
					  nautilus_file_info_list_copy (files));

			items = g_list_prepend (items, item);

			return items;
		}
	}

	return NULL;
}

static void nautilus_rename_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
	iface->get_background_items = nautilus_rename_get_background_items;
	iface->get_file_items = nautilus_rename_get_file_items;
}

static void nautilus_rename_instance_init (NautilusRename *img)
{
}

static void nautilus_rename_class_init (NautilusRenameClass *class)
{
}

GType nautilus_rename_get_type (void)
{
	return rename_type;
}

void nautilus_rename_register_type (GTypeModule *module)
{
	static const GTypeInfo info = {
		sizeof (NautilusRenameClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) nautilus_rename_class_init,
		NULL,
		NULL,
		sizeof (NautilusRename),
		0,
		(GInstanceInitFunc) nautilus_rename_instance_init,
	};

	static const GInterfaceInfo menu_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_rename_menu_provider_iface_init,
		NULL,
		NULL
	};

	rename_type = g_type_module_register_type (module,
						   G_TYPE_OBJECT,
						   "NautilusRename",
						   &info, 0);

	g_type_module_add_interface (module,
				     rename_type,
				     NAUTILUS_TYPE_MENU_PROVIDER,
				     &menu_provider_iface_info);
}
